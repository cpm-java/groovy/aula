package aula

class Exercicio1 {
	static main(args) {
		println("Hello World!")
		
		String nome = "Cassiano"
		int idade = 31
		
		String frase = "o " + nome + " tem " + idade + " anos."
		
		println(frase)
		
		String frase2 = "o $nome tem $idade anos."
		
		println(frase2)
	}
}
